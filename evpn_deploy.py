from nornir import InitNornir
from nornir.core.filter import F
from nornir.core.task import Task, Result
from nornir_napalm.plugins.tasks import napalm_configure
from nornir_utils.plugins.functions import print_title, print_result
from nornir_napalm.plugins.tasks import napalm_configure
import os
import sys
import urllib3

from evpn_build import get_nr, get_groups

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



def config_switches(task, CONFIG_DIR):
    print(f"Configuring {task.host.name}\n")

    with open(f"{CONFIG_DIR}/{task.host.name}.txt", 'r') as config_file:
        config = config_file.read()

    task.host['config'] = config

    # Deploy that configuration to the device using NAPALM
    r = task.run(task=napalm_configure,
             name="Loading Configuration on the device",
             replace=True,
             configuration=task.host["config"],
             )



def main():
        
    try:
        CONFIG_DIR = os.environ['CONFIG_DIR']
        print(f"\nCONFIG_DIR is set to {CONFIG_DIR}\n")
    except:
        print("\nPlease set CONFIG_DIR environment variable.\n")
        sys.exit()

    # initialize nornir and get groups from cli arguments
    nr = get_nr(CONFIG_DIR)
    group_list = get_groups()

    #host = nr.filter(children_of_group="test")
    print("Configuring the fabric...")
    nr = nr.filter(F(groups__all=group_list))
    result = nr.run(task=config_switches, CONFIG_DIR=CONFIG_DIR)
    print_result(result)



if __name__ == "__main__":
    main()
