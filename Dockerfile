FROM ubuntu

LABEL project=arista-evpn-code

# Update aptitude with new repo
RUN apt-get -yq update && apt-get install -y git python3.8 python3-pip

ADD . /arista-evpn-code
# change dir
WORKDIR /arista-evpn-code

# install modules
RUN pip3 install -r requirements.txt

RUN ln -s /usr/bin/python3.8 /usr/bin/python
RUN ln -s /usr/bin/pip3 /usr/bin/pip