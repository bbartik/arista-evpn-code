from nornir import InitNornir
from nornir.core.filter import F
from nornir.core.task import Task, Result
from nornir_jinja2.plugins.tasks import template_file
from nornir_napalm.plugins.tasks import napalm_configure
from nornir_utils.plugins.functions import print_title, print_result
from nornir_utils.plugins.tasks.data import load_yaml
from ruamel.yaml import YAML
from jinja2 import Environment, FileSystemLoader
import pprint
import os
import sys
import ipaddress
import argparse



def get_nr(CONFIG_DIR):
    """ 
    Return nr object with variable inventory dirs.
    """
    nr = InitNornir(
        core={
            "raise_on_error": True,
        },
        inventory={
            "options": {
                "host_file": f"{CONFIG_DIR}/inventory/hosts.yaml",
                "group_file": f"{CONFIG_DIR}/inventory/groups.yaml",
                "defaults_file": f"{CONFIG_DIR}/inventory/defaults.yaml", 
            }
        }

    )

    return nr



def get_groups():

    # set groups before running script
    parser=argparse.ArgumentParser()
    parser.add_argument('-g', help="Inventory groups", dest="groups", action='append', nargs='+')
    args = parser.parse_args()
    groups = args.groups

    # each item in groups is a one word list, use list comp. to make new list
    try:
        group_list = [ g[0] for g in groups ]
    except:
        print("Please configure group with -g first")
        print("Example: 'python evpn_build.py -g dev'\n")
        sys.exit()

    return group_list



def map_nodes(CONFIG_DIR, TEMPLATE_DIR, nr, group_list):
    """ 
    We create the model at run-time for the environment, such as dev or prod.
    This function maps nodes suchs as 'spine1' or 'leaf3' to their corresponding inventory name.
    This way the underlay models references the right inventory variables.
    For example, leaf1 is a node in the model, but it can be taken by host leaf1 host or leaf1_dev.    
    """

    # load the hostmap file with mappings of node: host
    yaml = YAML()
    with open(f'{CONFIG_DIR}/models/hostmap.yaml', 'r') as f:
        hostmap = yaml.load(f) 

    # we need to determine which nodes will be in the fabric model, prod or dev
    if "dev" in group_list:
        hostmap_key = "dev"
    elif "prod" in group_list:
        hostmap_key = "prod"
    else:
        print("No hostmap found, exiting.")
        sys.exit()

    # the mlag config requires we know the ip for keepalive, we set that here
    # to pass to the jinja2 template

    for k, v in hostmap[hostmap_key].items():
        host_key = v['host']
        hostmap[hostmap_key][k]["primary_ip"] = nr.inventory.hosts[host_key].hostname

    # load the template and render with values from hostmap data
    file_loader = FileSystemLoader(f'{CONFIG_DIR}/models')
    env = Environment(loader=file_loader)
    template = env.get_template('underlay.j2')
    output = template.render(nodes=hostmap[hostmap_key])
    
    # write the underlay model to file - we use this in make_device_model function
    with open(f"{CONFIG_DIR}/models/underlay.yaml", 'w+') as f:
        f.write(output)

    print(f"Underlay model created at {CONFIG_DIR}/models/underlay.yaml")



def make_device_model(task, CONFIG_DIR):
    """
    Parse the model data files and create device specfic models.
    We will be adding stuff to the task.host.data dictionary that we
    can pass to the jinj2 config template.
    """

    # store underlay data in a dict
    underlay_file = task.run(task=load_yaml, file=f"{CONFIG_DIR}/models/underlay.yaml")
    underlay = underlay_file[0].result

    # store overlay data in a dict
    overlay_file = task.run(task=load_yaml, file=f"{CONFIG_DIR}/models/overlay.yaml")
    overlay = overlay_file[0].result

    # initialize the device-specifc mlag construct
    mlag = {}

    # loop through each mlag dict, to find the matching one to get proper details
    '''
    {
        'local_ip': '10.4.0.2', 
        'peer_ip': '10.4.0.1', 
        'address': '10.3.0.2', 
        'domain': 'rack1'
    }
    '''
    for key, value in underlay['mlag_domains'].items():
        domain_id = key
        subnet = value['peer_subnet']
        l3_subnet = value['l3_subnet']
        mlag_ips = [ i for i in ipaddress.IPv4Network(subnet)]
        l3_ips = [ i for i in ipaddress.IPv4Network(l3_subnet)]
    
        # check to see if device is listed in primary or secondary peer field
        if task.host.name == (value['primary']):
            mlag.update({"local_ip": str(mlag_ips[1])})
            mlag.update({"peer_ip": str(mlag_ips[2])})
            mlag.update({"address": str(l3_ips[1])})
            mlag.update({"domain": domain_id})
            task.host.data['mlag'] = mlag
        if task.host.name == (value['secondary']):
            mlag.update({"local_ip": str(mlag_ips[2])})
            mlag.update({"peer_ip": str(mlag_ips[1])})
            mlag.update({"address": str(l3_ips[2])})
            mlag.update({"domain": domain_id})
            task.host.data['mlag'] = mlag

    # gather bgp neighbors for each spine
    if task.host.data['role'] == "spine":
        # get leaf ips for bgp 
        leafs = underlay['members']['leaf']
        leaf_ips = []
        for k, v in leafs.items():
            ip = v['loopbacks']['loopback0']['ip']
            leaf_ips.append(ip)
        task.host.data.update({"neighbors":leaf_ips})
    
    # gather bgp neighbors for each leaf
    if task.host.data['role'] == "leaf":
        # get spine ips for bgp 
        spines = underlay['members']['spine']
        spine_ips = []
        for k, v in spines.items():
            ip = v['loopbacks']['loopback0']['ip']
            spine_ips.append(ip)
        task.host.data.update({"neighbors":spine_ips})

    # get spine ips for anycast rp list
    spines = {node:v for node, v in underlay['members']['spine'].items()}  
    anycast_ips = []
    for k, v in spines.items():
        ip = v['loopbacks']['loopback0']['ip']
        anycast_ips.append(ip)
    task.host.data.update({"anycast_rps":anycast_ips})
      
    # initialize interface list for each node 
    task.host.data['interfaces'] = []
    task.host.data['portchannels'] = []

    # start with adding core links to the list
    core_links = [link for link in underlay['links']['core']]
    # convert this to a dict
    for link in core_links:
        #link = {k:v for k,v in link.items()}
        # find the links for this particular host and add to data model
        if task.host.name in link.values():
            interface_obj = {}
            if task.host.data['role'] == "spine":
                interface_obj['interface'] = link['downlink']
            elif task.host.data['role'] == "leaf":
                interface_obj['interface'] = link['uplink']
            # get ip address from subnet
            interface_subnet = ipaddress.ip_network(link['subnet'])
            interface_ips = [ ip for ip in interface_subnet ]
            if task.host.data['role'] == "spine":
                interface_ip = str(interface_ips[1])
            if task.host.data['role'] == "leaf":
                interface_ip = str(interface_ips[2])
            interface_mask =  str(interface_subnet.prefixlen)
            ip_mask = interface_ip + "/" + interface_mask
            interface_obj['address'] = ip_mask
            interface_obj['role'] = "underlay"
            task.host.data['interfaces'].append(interface_obj)

    # compile loopback interface list for each node
    task.host.data['loopbacks'] = []
    # filtering by roles, and then hostname to get to correct device
    role_members = { k:v for k, v in underlay['members'][task.host.data['role']].items() } 
    member = { k:v for k, v in role_members[task.host.name].items() }
    for k, v in member['loopbacks'].items():  
        loopback_obj = {}
        loopback_obj['interface'] = k
        loopback_obj['ip'] = v['ip']
        loopback_obj['desc'] = v['desc']
        task.host.data['loopbacks'].append(loopback_obj)

    # next we add overlay links to the list
    host_loc = task.host.data['loc']
    overlay_intfs = overlay['interfaces']
    if host_loc in overlay_intfs.keys():
        intf_to_add = overlay_intfs[host_loc]
        for k, v in intf_to_add.items():
            interface_obj={}
            interface_obj['interface'] = k
            interface_obj['vlan'] = v['vlan']
            interface_obj['role'] = "overlay"
            try:
                interface_obj['lag_id'] = v['mlag']
                # if a mlag is defined we need to add the mlag
                mlag_intf_obj={}
                mlag_intf_obj['interface'] = "port-channel" + str(v['mlag'])
                mlag_intf_obj['vlan'] = v['vlan']
                mlag_intf_obj['mlag'] =  v['mlag']
                mlag_intf_obj['role'] = "overlay"
                mlag_intf_obj['is_lag'] = "True"
                task.host.data['portchannels'].append(mlag_intf_obj)               
            except:
                None
            try:
                interface_obj['lag_id'] = v['po']
                # if a po is defined we need to add the port-channel
                po_intf_obj={}
                po_intf_obj['interface'] = "port-channel" + str(v['po'])
                po_intf_obj['vlan'] = v['vlan']
                po_intf_obj['role'] = "overlay"
                po_intf_obj['is_lag'] = "True"
                task.host.data['portchannels'].append(po_intf_obj)               
            except:
                None
            task.host.data['interfaces'].append(interface_obj)

    # add vpc peer links to the list
    peer_links = [link for link in underlay['links']['mlag_peer']]
    for peer_link in peer_links:
        if task.host.name in peer_link['peers']:
            intf_to_add= {}
            intf_to_add['interface'] = peer_link['interface']
            intf_to_add['lag_id'] = peer_link['lag']
            intf_to_add['vlan'] = 'all'
            intf_to_add['role'] = "peerlink"
            task.host.data['interfaces'].append(intf_to_add)   
            po_intf_obj = {}
            po_intf_obj['interface'] = "port-channel" + str(peer_link['lag'])
            po_intf_obj['role'] = "peerlink"
            task.host.data['portchannels'].append(po_intf_obj)  

    # compile vrf list for each node, adding management vrf to it, and sort
    task.host.data['vrfs'] = {}
    vrfs_to_add = overlay['vrfs']
    mgmt_vrf = {}
    mgmt_vrf = { 'management' : {} }
    mgmt_vrf['management']['routes'] = {}
    def_route = {}
    # optional, can write these in config
    def_route.update({
        "0.0.0.0/0": {
            "next_hops": [
                task.host['mgmt_gw'],
            ]
        }
     })
    mgmt_vrf['management']['routes'] = def_route
    vrfs_to_add.update(mgmt_vrf)
    # sort dict in alphabetical order for nxos config:
    for key in sorted(vrfs_to_add.keys()):
        vrf_obj = { key:vrfs_to_add[key] }
        task.host.data['vrfs'].update(vrf_obj)

    # compile vlan list for each node (leafs only)
    ''' EXAMPLE:
    {   
        110: {
            'name': 'vlan110', 
            'vni': 20001, 
            'vrf': 'tenant-a', 
            'address': '192.168.110.1/24', 
            'group': '239.0.51.10'
        }, 
        4093: {
            'name': 'MLAG_L3', 
            'group': 'MLAG_L3', 
            'address': '10.3.0.1/30'
        }
    }
    '''
    if task.host.data['role'] == "leaf":
        task.host.data['vlans'] = {}
        task.host.data['vlans'].update(overlay['vlans'])
        mlag_vlan = underlay['global']['peer_vlan']
        task.host.data['vlans'].update(
            {
                mlag_vlan: { 
                    "name": "MLAG_PEER",
                    "group": "MLAG_PEER",
                    "address": mlag['local_ip'] + "/30",
                }
            }
        )
        l3_vlan = underlay['global']['l3_vlan']
        task.host.data['vlans'].update(
            {
                l3_vlan: { 
                    "name": "MLAG_L3",
                    "group": "MLAG_L3",
                    "address": mlag['address'] + "/30",
                }
            }
        )

    # Transform inventory data to model file via a template file
    # task.host.data aready gets passed, we also need to pass the global underlay vars

    r = task.run(task=template_file,
                 name="Device Model",
                 template="device.j2",
                 path=f"{TEMPLATE_DIR}", fabric=underlay)

    with open(f"{CONFIG_DIR}/{task.host}.yaml", 'w+') as f:
        f.write(r.result)



def make_config(task, CONFIG_DIR):
    """
    This function uses host_vars files to create NXOS checkpoint config
    from the jinja2 templates.
    """

    # load extra vars from the data models
    vars_file = task.run(task=load_yaml,
                         file=f"{CONFIG_DIR}/{task.host.name}.yaml")
    vars = vars_file[0].result

    host_data = { k:v for k, v in task.host.items()}

    # Transform inventory data to model file via a template file
    r = task.run(task=template_file,
                 name="EOS Configuration",
                 template="eos.j2",
                 path=f"{TEMPLATE_DIR}", vars=vars)

    with open(f"{CONFIG_DIR}/{task.host}.txt", 'w+') as f:
        f.write(r.result)     



if __name__ == "__main__":

    # check is CONFIG_DIR is set, since config repo is separate    
    try:
        CONFIG_DIR = os.environ['CONFIG_DIR']
        print(f"\nCONFIG_DIR is set to {CONFIG_DIR}")
    except:
        print("\nPlease set CONFIG_DIR environment variable.\n")
        sys.exit()

    try:
        TEMPLATE_DIR = os.environ['TEMPLATE_DIR']
        print(f"TEMPLATE_DIR is set to {TEMPLATE_DIR}\n")
    except:
        print("\nPlease set TEMPLATE_DIR environment variable.\n")
        sys.exit()

    # initialize nornir and get groups from cli arguments
    nr = get_nr(CONFIG_DIR)
    group_list = get_groups()

    # map nodes to inventory hosts
    print("Creating the underlay model...")
    map_nodes(CONFIG_DIR, TEMPLATE_DIR, nr, group_list)

    # create all the host specific models based on group (dev or prod)
    print("Creating the device models")
    nr = nr.filter(F(groups__all=group_list))
    result = nr.run(task=make_device_model, CONFIG_DIR=CONFIG_DIR)

    print(f"Device models created in {CONFIG_DIR}/.")
    print("Creating device configuration files...")
    result = nr.run(task=make_config, CONFIG_DIR=CONFIG_DIR)

    print("Device configuration files created in {CONFIG_DIR}/.")
    print("\nPlease run 'python evpn_deploy.py' to deploy the configuration.\n")


