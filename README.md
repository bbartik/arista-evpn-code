# ARISTA EVPN PROJECT

This project uses Nornir to build and deploy EVPN configuration on an Arista spine-leaf topology. The configurations are contained in a separate repo at https://gitlab.com/bbartik/arista-evpn-config (aka "config repo").

![](topology.jpg) 

## Source of Truth

The following files in the config repo are used to generate the configs:

```
host:arista-evpn-config$ tree
.
├── inventory
│   ├── defaults.yaml
│   ├── groups.yaml
│   └── hosts.yaml
└── models
    ├── hostmap.yaml
    ├── overlay.yaml
    ├── underlay.j2
    └── underlay.yaml
```

- ./inventory is the location of the Nornir simple inventory files
- ./models are where are the variables and parameters for the network are stored
- hostmap.yaml maps each device in the inventory to a specific location in the fabric so that the same config and be applied to multiple environments for example. For example, "leaf11" is a location in the fabric taken up by either "leaf1" or "leaf1-dev" depending on the environment (group) being isued - see below for more.

## Use

First set two environment variables:
- CONFIG_DIR: to where you cloned the config repo
- TEMPLATE_DIR: set this to ./templates

There are two inventory groups in use: dev and prod. You specifcy them with the -g option.

```
arista-evpn-code$ python evpn_build.py -g dev
```

or

```
arista-evpn-code$ python evpn_build.py -g prod
```

Configs are outputted to the directory specified in the environment variable CONFIG_DIR.

## Staging

You must configure the switches to first as follows so the configs can be deployed via the EOS API.

```
conf t
service routing protocols model multi-agent
aaa authorization exec default local
username admin privilege 15 secret admin
vrf instance management
ip routing vrf management
interface Management1
   vrf management
   ip address {{ ip }
ip route vrf management 0.0.0.0/0 172.28.89.1
management api http-commands
   no shutdown
   vrf management
      no shutdown
end
wr mem
```
